Web Scraper
===========

Requirements
--------------
JDK 1.7+, Maven (tested using 3.3.3, other versions should work).

See `pom.xml` for list of libraries used.

Build Instructions
------------------
The following steps describe how to checkout, build and run the web scraper application.
Note: the build step (3) will run all tests against the code base.

1. `git clone https://bitbucket.org/hutchid1/webscraper`
2.  `cd webscraper`
3. `mvn clean package`

To run

1. `cd target`
2. `java -jar WebScraper-jar-with-dependencies.jar`

Overview
--------
The application design is simple and separates the production of a
'Report' from the formatting and display as a JSON object.

The `ReportProducer` interface describes the operation that implementing 
classes should provide, its only current implementation is the 
`WebScrapingReportProducer` which employs a heuristic approach to scraping
the web pages. The decision to abstract this element of the application
was primarily due to the various approaches and implementations that could be
taken to web scraping and if a better one is produced the application can be 
easily modified. 

The Report formatting is handled by the Jackson JSON serialisation library
the `ReportFormatter` interface asks implementors to output their result
to the specified OutputStream this should allow for other types of output to
be trivially supported in the future (such as to file rather than STDOUT).

Exception handling has been kept intentionally simple in this application,
primarily due to the relatively simple means by which errors can be reported
in a command line application (i.e. their exit code). Exceptions are
subsequently wrapped and caught by the `ConsoleApplication` class which handles
generation of an appropriate exit code, it helps human users by also printing
the full stack trace of the Exception that resulted in the error.

Discussion
-----------
There are a number of areas that could be improved:

1. Command line arguments could be introduced to allow users to specify 
   amongst other things, an alternate URL to generate a report on, the 
   number of threads the user wishes to allocate to the thread pool used 
   to fetch the detail pages and rather than outputting the content to STDOUT 
   a file path could be used to output the contents there also.

2. The test coverage of the `WebScrapingReportProducer` is less than desirable. 
   To allow injection of mockable instances into that class I've wrapped the Jsoup
   static interface. The same approach could then be followed
   to mock returned `Document` classes by the 3rd party library to better cover 
   the various branches of logic in that class.

