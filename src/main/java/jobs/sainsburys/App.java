package jobs.sainsburys;

import jobs.sainsburys.application.ConsoleApplication;

public class App {

    private App() {

    }

    public static void main(String[] args) {
        int returnCode = ConsoleApplication.run(args);

        // Act like a well behaved command line application reported our status
        // via our return code.
        System.exit(returnCode);
    }
}
