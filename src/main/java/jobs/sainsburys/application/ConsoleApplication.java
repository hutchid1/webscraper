package jobs.sainsburys.application;

import jobs.sainsburys.domain.Report;
import jobs.sainsburys.report.JacksonReportFormatter;
import jobs.sainsburys.report.ReportFormatter;
import jobs.sainsburys.report.ReportProducer;
import jobs.sainsburys.report.WebScrapingReportProducer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleApplication {

    private static final Logger log = LoggerFactory.getLogger(ConsoleApplication.class);

    private static final String PRODUCTS_URI =
            "http://hiring-tests.s3-website-eu-west-1.amazonaws.com/2015_Developer_Scrape/5_products.html";

    public static int run(String [] args) {
        try {
            // Produce the report.
            ReportProducer reportProducer = new WebScrapingReportProducer();
            Report report = reportProducer.produce(PRODUCTS_URI);

            // Output report to STDOUT.
            ReportFormatter reportFormatter = new JacksonReportFormatter();
            reportFormatter.format(report, System.out);
        } catch(Exception exception) {
            // Provide simple for of crash reporting
            log.error("Error occurred generating report", exception);
            return 1;
        }

        return 0;
    }
}
