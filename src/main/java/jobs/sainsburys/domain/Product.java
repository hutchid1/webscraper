package jobs.sainsburys.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Encapsulates information about a product which has been scraped from the
 * Sainsbury's shopping site.
 *
 * Because the information about a product is spread across two pages (the
 * listing page and detail page) we use a 'builder' to store the partial
 * information about a Product and then use its build() method to create
 * a fully initialised Product instance.
 */
public class Product {
    private final String title;

    @JsonProperty("unit_price")
    private final BigDecimal unitPrice;

    @JsonProperty("size")
    private final long linkedSize;

    private final String description;

    private Product(String title, BigDecimal unitPrice, long linkedSize, String description) {
        this.title = title;
        this.unitPrice = unitPrice;
        this.linkedSize = linkedSize;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public BigDecimal getUnitPrice() {
        return unitPrice;
    }

    /**
     * @return the size of the detail page (in kb) for this Product.
     */
    public String getLinkedSize() {
        return (linkedSize / 1024) + "kb";
    }

    public String getDescription() {
        return description;
    }

    /**
     * Builder class for the Product class.
     */
    public static class Builder {
        private String title;
        private BigDecimal unitPrice;
        private long linkedSize;
        private String description;

        public Builder() {
            this.title = null;
            this.unitPrice = null;
            this.linkedSize = -1;
            this.description = null;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public void setUnitPrice(BigDecimal unitPrice) {
            this.unitPrice = unitPrice;
        }

        public void setLinkedSize(long linkedSize) {
            this.linkedSize = linkedSize;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        /**
         * Builds a fully initialised instance of a Product class.
         * @return an instance of a Product class
         * @throws IllegalArgumentException if any fields have not been set
         *  before calling build().
         */
        public Product build() {
            if(title == null || unitPrice == null || linkedSize < 0 || description == null)
                throw new IllegalArgumentException("Error building Product, all fields must be specified.");

            return new Product(title, unitPrice, linkedSize, description);
        }
    }
}
