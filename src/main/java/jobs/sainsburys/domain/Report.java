package jobs.sainsburys.domain;

import com.google.common.collect.Lists;

import java.math.BigDecimal;
import java.util.List;

/**
 * Provides a simple Report about a collection of Product that have been
 * added to it.
 */
public class Report {
    private List<Product> results;
    private BigDecimal total;

    /**
     * Creates a new empty report.
     */
    public Report() {
        this.results = Lists.newArrayList();
        this.total = BigDecimal.ZERO;
    }

    /**
     * Adds a Product to this Report.
     */
    public void addProduct(Product product) {
        results.add(product);

        // Maintain a running total.
        total = total.add(product.getUnitPrice()).setScale(2);

    }

    public List<Product> getResults() {
        return results;
    }

    public BigDecimal getTotal() {
        return total;
    }
}
