package jobs.sainsburys.report;

import com.google.common.collect.Lists;
import jobs.sainsburys.domain.Product;
import jobs.sainsburys.domain.Report;
import jobs.sainsburys.report.scraper.JSoupWrapper;
import org.jsoup.Connection;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.*;

/**
 * Implementation of ReportProducer that uses Jsoup to scape the specified
 * resource and produce a Report based on the business logic described in
 * "Software Engineer Technical Test.pdf"
 */
public class WebScrapingReportProducer implements ReportProducer {

    private static final int THREAD_POOL_SIZE = 4;

    private JSoupWrapper jSoupWrapper;

    public WebScrapingReportProducer() {
        jSoupWrapper = new JSoupWrapper();
    }

    public WebScrapingReportProducer(JSoupWrapper jSoupWrapper) {
        this.jSoupWrapper = jSoupWrapper;
    }

    @Override
    public Report produce(String uri) throws ReportProductionException {
        /**
         * 1). Obtain the product list page
         * 2). Get a list of all products using the 'product' CSS class
         * 3). Create a Product Builder for each then dispatch to the Thread
         *     pool to gather details from the page itself.
         * 4). Gather all results to product final Report
         */
        Document productListPage = getProductListPage(uri);

        ExecutorService threadPool = Executors.newFixedThreadPool(THREAD_POOL_SIZE);
        List<Future<Product.Builder>> futures = Lists.newArrayList();

        Elements productElements = productListPage.getElementsByClass("product");
        for(Element productElement : productElements) {
            // We can get the title and unit price from this section of the DOM
            Product.Builder builder = new Product.Builder();

            // Product title & URI we need to follow.
            Elements productInformation = productElement.select("a[href]");
            if(productInformation.size() == 0)
                throw new ReportProductionException("Product page missing required HTML markup");

            builder.setTitle(productInformation.first().text().trim());
            String productDetailUri = productInformation.first().attr("abs:href");

            // Unit price
            Elements pricePerUnitElements = productElement.select("p.pricePerUnit");
            if(pricePerUnitElements.size() == 0)
                throw new ReportProductionException("Product page missing required price per unit");

            String pricePerUnit = pricePerUnitElements.first().text().replaceAll("[^0-9.]", "");
            builder.setUnitPrice(BigDecimal.valueOf(Double.parseDouble(pricePerUnit)).setScale(2));

            // Create a new job to fetch the rest of the information.
            futures.add(threadPool.submit(new ProductBuilderJob(productDetailUri, builder)));
        }

        // Collect the results from each future then return the result.
        Report result = new Report();
        for(Future<Product.Builder> future : futures) {
            try {
                Product.Builder builder = future.get();
                result.addProduct(builder.build());
            } catch (InterruptedException | ExecutionException e) {
                throw new ReportProductionException("Error occurred retrieving detailed product information.", e);
            }
        }

        return result;
    }


    Document getProductListPage(String uri) throws ReportProductionException {
        try {
            return jSoupWrapper.connect(uri).get();
        } catch(IOException exception) {
            throw new ReportProductionException("Error occurred retrieving product list page", exception);
        }
    }

    private class ProductBuilderJob implements Callable<Product.Builder> {

        private String detailPageUri;
        private Product.Builder productBuilder;

        public ProductBuilderJob(String detailPageUri, Product.Builder builder) {
            this.detailPageUri = detailPageUri;
            this.productBuilder = builder;
        }

        @Override
        public Product.Builder call() throws Exception {
            // Fetch the resource and obtain its size in bytes.
            Connection connection = WebScrapingReportProducer.this.jSoupWrapper.connect(detailPageUri);
            Connection.Response response = connection.execute();
            productBuilder.setLinkedSize(response.bodyAsBytes().length);

            // Now parse it and find the product's description.
            // The description is less 'semantically well' marked up, the heuristics
            // are essentially:
            //  - of 'productText' class
            //  - sibling of <h3> tag with text 'Description'
            //  - first 'group' of this kind on detail page.
            //
            // For this exercise we use the 3rd heuristic - the fact that it
            // is the first element of the productText class in the DOM.
            Document productDescriptionDocument = response.parse();
            Elements productDescriptionElements = productDescriptionDocument.select("div.productText");
            if(productDescriptionElements.size() == 0)
                throw new ReportProductionException("Missing product detail information.");

            productBuilder.setDescription(productDescriptionElements.first().text());

            return this.productBuilder;
        }
    }
}
