package jobs.sainsburys.report;


import jobs.sainsburys.domain.Report;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface to abstract the means by which a Report instance
 * is serialised and output.
 *
 * This allows for XML or CSV report types to be easily added
 * in future without significant change to the rest of the
 * application.
 */
public interface ReportFormatter {

    /**
     * Serialise the Report instance and output to the OutputStream provided.
     *
     * @param report Report instance to serialise and output
     * @param outputStream OutputStream to use when outputting the Report.
     */
    void format(Report report, OutputStream outputStream) throws IOException;

}
