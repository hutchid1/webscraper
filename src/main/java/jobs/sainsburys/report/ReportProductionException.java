package jobs.sainsburys.report;

/**
 * Thrown during the production of a Report when errors are encountered
 * fetching or preparing the Report.
 */
public class ReportProductionException extends Exception {

    public ReportProductionException(String message) {
        super(message);
    }

    public ReportProductionException(String message, Throwable cause) {
        super(message, cause);
    }
}
