package jobs.sainsburys.report;

import jobs.sainsburys.domain.Report;

/**
 * Implementors should produce a Report based on the provided URI.
 *
 * The implementor is free to choose the method of retrieving the specified
 * resource and how it is parsed.
 */
public interface ReportProducer {

    /**
     * Implementors should retrieve the requested resource, parse it and
     * return the completed Report.
     *
     * Partial Reports should not be returned, if an error occurs obtaining
     * or processing the resource then a ReportProductionException should
     * be thrown.
     *
     * @param uri the URI of the resource about which the Report should be
     *            generated
     * @return A Report instance.
     * @throws ReportProductionException
     */
    Report produce(String uri) throws ReportProductionException;
}
