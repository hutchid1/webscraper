package jobs.sainsburys.report.scraper;

import org.jsoup.Connection;
import org.jsoup.Jsoup;

/**
 * A wrapper around the Jsoup static interface class to effectively translate
 * static method calls in instance method calls to allow easier mocking
 * and testability.
 */
public class JSoupWrapper {

    public Connection connect(String uri) {
        return Jsoup.connect(uri);
    }

}
