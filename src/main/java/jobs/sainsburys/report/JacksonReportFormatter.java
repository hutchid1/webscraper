package jobs.sainsburys.report;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import jobs.sainsburys.domain.Report;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Implementation of ReportFormatter that uses the Jackson JSON serialisation
 * library to produce a JSON representation of the specified Report.
 */
public class JacksonReportFormatter implements ReportFormatter {

    @Override
    public void format(Report report, OutputStream outputStream) throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        ObjectWriter objectWriter = objectMapper.writerWithDefaultPrettyPrinter();

        objectWriter.writeValue(outputStream, report);
    }
}
