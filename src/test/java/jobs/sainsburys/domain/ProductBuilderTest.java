package jobs.sainsburys.domain;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProductBuilderTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void givenAllFieldsAreSpecifiedwhenBuildCalledthenValidProductInstanceResturned() {
        Product.Builder builder = new Product.Builder();

        builder.setTitle("Test Title");
        builder.setUnitPrice(BigDecimal.ONE);
        builder.setLinkedSize(1024);
        builder.setDescription("Test Description");

        Product product = builder.build();
        assertThat(product.getTitle(), equalTo("Test Title"));
        assertThat(product.getUnitPrice(), equalTo(BigDecimal.ONE));
        assertThat(product.getLinkedSize(), equalTo("1kb"));
        assertThat(product.getDescription(), equalTo("Test Description"));
    }

    @Test
    public void givenTitleFieldIsNotSpecifiedwhenBuildCalledthenThrowIllegalArgumentException() {
        expectedException.expect(IllegalArgumentException.class);

        Product.Builder builder = new Product.Builder();

        builder.setUnitPrice(BigDecimal.ONE);
        builder.setLinkedSize(1024);
        builder.setDescription("Test Description");

        Product product = builder.build();
    }
}