package jobs.sainsburys.domain;

import org.junit.Test;

import java.math.BigDecimal;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasSize;

public class ReportTest {

    @Test
    public void givenTwoProdcutsAddedwhenRunningTotalCheckedthenCorrectValuesReturned() {
        Product.Builder builder = new Product.Builder();
        builder.setTitle("Test 1");
        builder.setUnitPrice(BigDecimal.valueOf(3.50));
        builder.setLinkedSize(1024);
        builder.setDescription("Test");

        Product product1 = builder.build();

        builder.setTitle("Test 2");
        builder.setUnitPrice(BigDecimal.valueOf(1.50));

        Product product2 = builder.build();

        // Exercise our code.
        Report report = new Report();
        report.addProduct(product1);

        assertThat(report.getTotal(), equalTo(BigDecimal.valueOf(3.50).setScale(2)));
        assertThat(report.getResults(), hasSize(1));

        report.addProduct(product2);

        assertThat(report.getTotal(), equalTo(BigDecimal.valueOf(5.00).setScale(2)));
        assertThat(report.getResults(), hasSize(2));
    }
}