package jobs.sainsburys.report;

import jobs.sainsburys.report.scraper.JSoupWrapper;
import org.jsoup.Connection;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.IOException;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class WebScrapingReportProducerTest {

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Mock
    private JSoupWrapper jSoupWrapper;

    @Mock
    private Connection connection;

    @Test
    public void givenIOExceptionwhenProduceCalledThenExceptReportProductionException() throws ReportProductionException, IOException {
        expectedException.expect(ReportProductionException.class);

        when(connection.get()).thenThrow(IOException.class);
        when(jSoupWrapper.connect(anyString())).thenReturn(connection);

        WebScrapingReportProducer webScrapingReportProducer =
                new WebScrapingReportProducer(jSoupWrapper);

        webScrapingReportProducer.produce("http://aduduri.com");
    }

}